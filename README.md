In the project directory:

```
npm i
npm start
```

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

This app was written in a manner that is easy to read, and meets all requirements at a very basic level. Redux was used purely for the purpose of showcase.