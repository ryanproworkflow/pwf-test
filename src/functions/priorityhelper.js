export const priorityhelper = (value) => {
    let result;
    switch (value){
        case 1:
            result = "Very Low";
            break;
        case 2:
            result = "Low";
            break;
        case 3:
            result = "Medium";
            break;
        case 4:
            result = "High";
            break;
        case 5:
            result = "Very High";
            break;
        default:
            result = "-"
            break;
    }
    return result
}