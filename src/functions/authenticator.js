import users from '../data/users.json';

export const authenticator = (username, pass) => {
    return users.find(
        user => 
            user.username === username
            && user.password === pass
        )
}
