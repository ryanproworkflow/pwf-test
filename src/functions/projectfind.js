import projects from '../data/projects.json';

export const projectfind = (projectNumber) => {
    return projects.find(
        project => 
            project.number === projectNumber
        )
}
