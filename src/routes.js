import React from 'react';
import { Route, Switch } from "react-router-dom";
import Home from './views/home';
import Login from './views/login'
import ProjectDetails from './views/projectdetails'

export default () =>
  <Switch>
    <Route path="/" exact component={Login} />
    <Route path="/home" exact component={Home} />
    <Route path="/projectdetails" exact component={ProjectDetails} />
  </Switch>;