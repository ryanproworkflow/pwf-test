import { combineReducers } from 'redux';
import login from './login';
import activeproject from './activeproject';
import keyword from './keyword';
import searchresults from './searchresults';


const rootReducer = combineReducers({
    login,
    activeproject,
    keyword,
    searchresults
})

export default rootReducer