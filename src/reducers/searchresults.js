export default(state = null, action) => {
    switch(action.type) {
        case 'DISPLAY_LIST_UPDATED':
            return action.displayList
        default:
            return state;
    }
};