export default(state = null, action) => {
    switch(action.type) {
        case 'KEYWORD_UPDATED':
            return action.keyword
        default:
            return state;
    }
};