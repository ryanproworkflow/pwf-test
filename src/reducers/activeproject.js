export default(state = null, action) => {
    switch(action.type) {
        case 'PROJECT_SELECTED':
            return action.projectNumber
        default:
            return state;
    }
};