import React, { Component } from 'react';
import '../styles/Home.scss';
import projects from '../data/projects.json';

import ProjectTable from '../components/home/projecttable';
import ProjectItem from '../components/home/projectitem';
import HeaderBar from '../components/common/headerBar';
import SearchBar from '../components/home/searchbar';


import { selectproject } from '../actions/selectProject';
import { keyword } from '../actions/keyword';
import { searchResults } from '../actions/searchResults';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { authenticator } from '../functions/authenticator';
import { loginchecker } from '../actions/login';


class Home extends Component {
  componentDidMount(){
      if (authenticator(localStorage.getItem('username'), localStorage.getItem('password'))){
        this.props.loginStatus(true)
        this.props.history.push('/home')
      }
  }

  projectList = (list) => {
    const results = list.map((project) =>
      <ProjectItem key={project.number} project={project} projectSelect={this.onProjectSelect}/>
    )
    return results
  }

  handleChange = (e) => {
      this.props.onSearch(e)
      let list;
      if (e){
        list = projects.filter(project => 
          project.title.toLowerCase().includes(e.toLowerCase())
          || project.company.name.toLowerCase().includes(e.toLowerCase())
          || project.number.toString().includes(e)
          || project.manager.fullname.toLowerCase().includes(e.toLowerCase())
        )
        this.props.updateResults(list)
    } else if (e === ""){
        this.props.updateResults(projects)
    }
  }

  onProjectSelect = (e) => {
    this.props.onSelectProject(e)
    this.props.history.push('/projectdetails')
  }
  
  render() {
    return (
      <div>
        <HeaderBar history={this.props.history} />
        <div className="home-page-container">
          <div className="search-bar">
            <SearchBar onChange={this.handleChange} keyword={this.props.keyword}/>
          </div>
          <ProjectTable projectEntries={this.projectList(this.props.displayList ? this.props.displayList : projects)}/>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return  {
      keyword: state.keyword,
      displayList: state.searchresults,
      loggedIn: state.login
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
      onSelectProject: bindActionCreators(selectproject,dispatch),
      onSearch: bindActionCreators(keyword, dispatch),
      updateResults: bindActionCreators(searchResults,dispatch),
      loginStatus: bindActionCreators(loginchecker,dispatch)
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);