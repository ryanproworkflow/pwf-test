import React, { Component } from 'react';
import '../styles/Login.scss';

import LoginForm from '../components/login/loginform';

import { authenticator } from '../functions/authenticator';

import { loginchecker } from '../actions/login';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

class Login extends Component {
  constructor(props){
    super(props);
    this.state={
        showFailedMessage:false,
        loginAttempted:false
    };
  }

  componentDidMount(){
    this.tryLogin()
  }

  tryLogin = () => {
    if (authenticator(localStorage.getItem('username'), localStorage.getItem('password'))){
      this.props.loginStatus(true)
      this.props.history.push('/home')
    }
  }

  loginClicked = () => {
    this.setState({
      loginAttempted:true
    })
    if(localStorage.getItem('username') && localStorage.getItem('password')){
        if (authenticator(localStorage.getItem('username'), localStorage.getItem('password'))){
          this.props.loginStatus(true)
          this.props.history.push('/home')
        } else {
        this.setState({
          showFailedMessage: true
        })
        localStorage.clear()
      }
    } else {
      this.setState({
        showFailedMessage: true
      })
      localStorage.clear()
    }
  }

  onChange = (type, event) => {
    localStorage.setItem(type, event.target.value)
  }

  render() {
    let errorMessage;

    if (this.state.showFailedMessage && this.state.loginAttempted){
      errorMessage = "Your credentials were incorrect."

      setTimeout(() => {
        this.setState({
            showFailedMessage: false,
        }) 
        },3500)
    }

    return (
      <div className="login-page">
        <LoginForm changeHandler={this.onChange} loginFunction={this.loginClicked} errorMessage={errorMessage}/>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return  {
      loggedIn: state.login
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
      loginStatus: bindActionCreators(loginchecker,dispatch)
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
