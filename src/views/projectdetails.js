import React, { Component } from 'react';
import '../styles/ProjectDetails.scss';

import HeaderBar from '../components/common/headerBar';
import Details from '../components/projectdetails/details';

import { authenticator } from '../functions/authenticator';
import { projectfind } from '../functions/projectfind';

import { loginchecker } from '../actions/login';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

class ProjectDetails extends Component {
  constructor(props){
    super(props);
    this.state={
        project: {}
    };
  }

  componentDidMount(){
    if (authenticator(localStorage.getItem('username'), localStorage.getItem('password'))){
      this.props.loginStatus(true)
    }
    this.setState({
      project:projectfind(this.props.currentProject)
    })
  }

  render() {
    let projectBody = this.state.project 
      ? <Details content={this.state.project}/>
      : 'Project not found'

    return (
      <div>
        <HeaderBar history={this.props.history} />
        <div className="project-details-container">
          {projectBody}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return  {
    currentProject:state.activeproject,
      loggedIn: state.login
  }
}

const mapDispatchToProps = (dispatch) => {
  return  {
      loginStatus: bindActionCreators(loginchecker,dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProjectDetails);