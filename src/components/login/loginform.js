import React, { Component } from 'react';
import '../../styles/Login.scss';
import { Button, Input } from 'reactstrap';

export default class LoginForm extends Component {
    render() {
      return (
        <div className="login-form">
            <h1>Login</h1>
            <Input placeholder="Username" onChange={(e) => this.props.changeHandler('username', e)}/>
            <Input placeholder="Password" type="password" onChange={(e) => this.props.changeHandler('password', e)}/>
            <Button className="login-button" onClick={this.props.loginFunction}>Login</Button>
            <p>{this.props.errorMessage}</p>
        </div>
      );
    }
  }