import React, { Component } from 'react';
import '../../styles/HeaderBar.scss';

import { Navbar,Nav,NavItem,NavLink } from 'reactstrap';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { loginchecker } from '../../actions/login';

class HeaderBar extends Component {
  onLogoutClick = () => {
    this.props.loginStatus(false)
    localStorage.clear()
    this.props.history.push('/')
  }

  render() {

    let logoutButton = this.props.loggedIn 
        ? <Nav className="ml-auto header-bar" navbar>
            <NavItem>
              <NavLink onClick={()=>this.props.history.push('/home')}>Projects</NavLink>
            </NavItem>
            <NavItem>
              <NavLink onClick={this.onLogoutClick}>Logout</NavLink>
            </NavItem>
          </Nav>
      : undefined

    return (
      <div>
        <Navbar color="light" light expand="md">  
            {logoutButton}
        </Navbar>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return  {
      loggedIn: state.login
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
      loginStatus: bindActionCreators(loginchecker,dispatch)
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(HeaderBar);
