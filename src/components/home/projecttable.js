import React, { Component } from 'react';
import '../../styles/Home.scss';
import { Table } from 'reactstrap';

export default class ProjectTable extends Component {
    render() {
      return (
        <div className="project-list">
          <Table>
            <thead>
              <tr className="heading">
                <th>No.</th>
                <th>Title</th>
                <th>Company</th>
                <th>Start Date</th>
                <th>Due Date</th>
                <th>Manager</th>
              </tr>
            </thead>
            <tbody>
              {this.props.projectEntries}
            </tbody>
          </Table>
        </div>
      );
    }
  }