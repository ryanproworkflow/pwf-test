import React, { Component } from 'react';
import '../../styles/Home.scss';
import { InputGroup, Input } from 'reactstrap';

export default class SearchBar extends Component {
    render() {
      return (
            <div>
                <InputGroup>
                    <Input placeholder="Search..." onChange={(e) => this.props.onChange(e.target.value)}/>
                </InputGroup>
            </div>
      );
    }
  }