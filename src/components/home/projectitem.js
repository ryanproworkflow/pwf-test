import React, { Component } from 'react';
import '../../styles/Home.scss';
import * as moment from 'moment';

export default class ProjectItem extends Component {
    render() {
      let startDate = this.props.project.startdate? new Date(this.props.project.startdate):undefined
      let dueDate = this.props.project.duedate ? new Date(this.props.project.duedate):undefined

      return (
            <tr onClick={() => this.props.projectSelect(this.props.project.number)}>
                <td>{this.props.project.number}</td>
                <td>{this.props.project.title}</td>
                <td>{this.props.project.company.name}</td>
                <td>{startDate? moment(startDate).format('DD MMM YYYY'): "-"}</td>
                <td>{dueDate? moment(dueDate).format('DD MMM YYYY'): "-"}</td>
                <td>{this.props.project.manager.fullname}</td>
            </tr>
      );
    }
  }