import React, { Component } from 'react';
import '../../styles/ProjectDetails.scss';

export default class DetailItem extends Component {
    render() {
      return (
            <div className="detail-item-container">
              <div className="detail-item">
                <h1>{this.props.title}</h1>
                <h2>{this.props.value}</h2>
                </div>
            </div>
      );
    }
  }