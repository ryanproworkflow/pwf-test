import React, { Component } from 'react';
import '../../styles/ProjectDetails.scss';
import { Container, Row, Col } from 'reactstrap';
import DetailItem from './detailitem';
import * as moment from 'moment';
import ReactHtmlParser from 'react-html-parser';
import { priorityhelper } from '../../functions/priorityhelper';

export default class Details extends Component {

  render() {
    let startDate = this.props.content.startdate? new Date(this.props.content.startdate):undefined
    let dueDate = this.props.content.duedate ? new Date(this.props.content.duedate):undefined
    let priority = this.props.content.priority ? priorityhelper(this.props.content.priority):'-'
    return (
          <div className="details">
            <h1>
              {this.props.content.number} - {this.props.content.title}
            </h1>
            <h2>
              {this.props.content.company ? this.props.content.company.name:""}
            </h2>
              <div className="detail-container">
                <Container>
                  <Row>
                    <Col>
                    <DetailItem title={'Managed by'} value={this.props.content.manager ? this.props.content.manager.fullname:""}/>
                    </Col>    
                      <Col>
                      <DetailItem title={'Time Allocated'} value={this.props.content.timeallocated ? this.props.content.timeallocated/60+" hours":" - "}/>
                    </Col>
                    <Col>
                      <DetailItem title={'Priority'} value={priority}/>
                    </Col> 
                  </Row>
                  <Row>
                    <Col>
                    </Col>
                    <Col>
                        <DetailItem title={'Start Date'} value={startDate? moment(startDate).format('DD MMM YYYY'): "-"}/>
                    </Col>
                    <Col>
                        <DetailItem title={'Due Date'} value={dueDate? moment(dueDate).format('DD MMM YYYY'): "-"}/>
                    </Col>
                  </Row>
                </Container> 
              </div>
              <div className="description-box">
                <DetailItem title={'Description'} value={this.props.content.description? ReactHtmlParser(this.props.content.description): " - "}/>
              </div>
              
          </div>
    );
  }
}