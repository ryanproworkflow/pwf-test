export const searchResults = (newQuery) => {
    return {
        type: 'DISPLAY_LIST_UPDATED',
        displayList: newQuery
        }
    }
