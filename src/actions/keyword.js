export const keyword = (newQuery) => {
    return {
        type: 'KEYWORD_UPDATED',
        keyword: newQuery
        }
    }
